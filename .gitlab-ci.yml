variables:
  IMAGE_ID: 'latest'
  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"

  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  BST_CACHE_SERVER_ADDRESS: 'cache.freedesktop-sdk.io'
  # mesa-git-extension is only available through Flathub beta
  RELEASE_CHANNEL: beta

default:
  image: "${DOCKER_REGISTRY}/bst2:${IMAGE_ID}"
  interruptible: true
  after_script:
  - rm -rf "${XDG_CACHE_HOME}/buildstream/artifacts"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/build"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/cas"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/sources"
  - rm -rf "${CI_PROJECT_DIR}/.bst"
  retry:
    max: 2
    when: runner_system_failure

stages:
- build
- publish
- update

workflow:
  rules:
  - if: '$CI_MERGE_REQUEST_IID'
  - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == "true"'
  - if: '$CI_COMMIT_TAG'
  - if: '$CI_PIPELINE_SOURCE == "schedule"'

before_script:
  - |
    export REPO_TOKEN="${FLATHUB_REPO_TOKEN_BETA}"

  - mkdir -p ~/.config
  - mkdir -p /etc/ssl/CAS

  # Private SSL keys/certs for pushing to the CAS server
  - |
    if [ -n "$BB_CAS_PUSH_CERT" ] && [ -n "$BB_CAS_PUSH_KEY" ]; then
      echo "$BB_CAS_PUSH_CERT" > /etc/ssl/CAS/server.crt
      echo "$BB_CAS_PUSH_KEY" > /etc/ssl/CAS/server.key
      {
        echo "artifacts:"
        echo "  override-project-caches: true"
        echo "  servers:"
        if [ -f /cache-certificate/server.crt ]; then
          echo "  - url: https://local-cas-server:1102"
          echo "    push: true"
          echo "    auth:"
          echo "      client-key: /etc/ssl/CAS/server.key"
          echo "      client-cert: /etc/ssl/CAS/server.crt"
          echo "      server-cert: /cache-certificate/server.crt"
        fi
        echo "  - url: https://${BST_CACHE_SERVER_ADDRESS}:11002"
        echo "    push: true"
        echo "    auth:"
        echo "      client-key: /etc/ssl/CAS/server.key"
        echo "      client-cert: /etc/ssl/CAS/server.crt"
        echo "source-caches:"
        echo "  override-project-caches: true"
        echo "  servers:"
        if [ -f /cache-certificate/server.crt ]; then
          echo "  - url: https://local-cas-server:1102"
          echo "    push: true"
          echo "    auth:"
          echo "      client-key: /etc/ssl/CAS/server.key"
          echo "      client-cert: /etc/ssl/CAS/server.crt"
          echo "      server-cert: /cache-certificate/server.crt"
        fi
        echo "  - url: https://${BST_CACHE_SERVER_ADDRESS}:11002"
        echo "    push: true"
        echo "    auth:"
        echo "      client-key: /etc/ssl/CAS/server.key"
        echo "      client-cert: /etc/ssl/CAS/server.crt"
      } >> ~/.config/buildstream.conf
    fi


.merge_request_build:
  stage: build
  except:
  - master@freedesktop-sdk/mesa-git-extension
  - /^release\/.*/@freedesktop-sdk/mesa-git-extension
  script:
  -  make build ARCH=${ARCH}
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs

.builder-x86_64:
  tags:
  - x86_64
  variables:
    ARCH: x86_64

.builder-i686:
  tags:
  - x86_64
  variables:
    ARCH: i686

build_x86_64:
  extends:
  - .merge_request_build
  - .builder-x86_64

build_i686:
  extends:
  - .merge_request_build
  - .builder-i686

.protected_build:
  only:
  - master@freedesktop-sdk/mesa-git-extension
  - /release\/.*/@freedesktop-sdk/mesa-git-extension

prepare_publish:
  extends:
  - .protected_build
  stage: publish
  script:
  - flat-manager-client create https://hub.flathub.org/ "${RELEASE_CHANNEL}" --build-log-url ${CI_PIPELINE_URL} > publish_build.txt
  except:
  - schedules
  artifacts:
    paths:
    - publish_build.txt

.publish_template:
  stage: publish
  interruptible: false
  variables:
    DISABLE_CACHE_PUSH: 1
  script:
  - make export-repo RELEASE_KIND=${RELEASE_CHANNEL} REPO=repo
  - flatpak build-update-repo repo
  - flat-manager-client push $(cat publish_build.txt) repo --build-log-url ${CI_PIPELINE_URL}
  needs:
  - prepare_publish
  artifacts:
    when: always
    paths:
    - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
  - schedules

publish_x86_64:
  extends:
  - .protected_build
  - .publish_template
  - .builder-x86_64

publish_i686:
  extends:
  - .protected_build
  - .publish_template
  - .builder-i686

.finish_publish_base:
  extends:
  - .protected_build
  stage: publish
  needs:
  - prepare_publish
  - job: publish_x86_64
    artifacts: false
  - job: publish_i686
    artifacts: false
  except:
  - schedules

finish_publish:
  extends:
  - .finish_publish_base
  script:
  - flat-manager-client commit --wait $(cat publish_build.txt)
  - flat-manager-client publish --wait $(cat publish_build.txt)
  - flat-manager-client purge $(cat publish_build.txt)

finish_publish_failed:
  extends:
  - .finish_publish_base
  script:
  - flat-manager-client purge $(cat publish_build.txt)
  when: on_failure

track_updates:
  stage: update
  only:
  - schedules
  script:
  - git remote set-url origin "https://gitlab-ci-token:${FREEDESKTOP_API_KEY}@gitlab.com/freedesktop-sdk/mesa-git-extension.git"
  - git config user.name "freedesktop_sdk_updater"
  - git config user.email "freedesktop_sdk_updater@libreml.com"
  - git branch -f "${CI_COMMIT_REF_NAME}" "origin/${CI_COMMIT_REF_NAME}"
  - |
    case "${CI_COMMIT_REF_NAME}" in
      master)
      ;;
      release/*)
      ;;
      *)
        false
      ;;
    esac
  - GITLAB_TOKEN=$FREEDESKTOP_API_KEY auto_updater --verbose
    --base_branch "${CI_COMMIT_REF_NAME}"
    --nobuild
    --overwrite
    --push
    --create_mr
    --gitlab_project="freedesktop-sdk/mesa-git-extension"
    --max_merge_requests=4
    track-elements.bst
    freedesktop-sdk.bst

